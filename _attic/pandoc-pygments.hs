-- A Pandoc filter to use Pygments for Pandoc
-- Code blocks in HTML output
-- Nickolay Kudasov 2013
-- Requires Pandoc 1.12

import Text.Pandoc.Definition
import Text.Pandoc.JSON (toJSONFilter)
import Text.Pandoc.Shared
import Data.Char(toLower)
import System.Process (readProcess)
import System.IO.Unsafe
import Control.Applicative

main = toJSONFilter highlight

highlight :: Block -> IO Block
highlight (CodeBlock (_, options , _ ) code) =
  RawBlock (Format "html") <$> (pygments code options)
highlight x = return x

pygments :: String -> [String] -> IO String
pygments code options
         | (length options) == 1 = readProcess "pygmentize" ["-l", (map toLower (head options)),  "-f", "html"] code
         | (length options) == 2 = readProcess "pygmentize" ["-l", (map toLower (head options)), "-O linenos=inline",  "-f", "html"] code
         | otherwise = readProcess "pygmentize" ["-l", "text",  "-f", "html"] code
